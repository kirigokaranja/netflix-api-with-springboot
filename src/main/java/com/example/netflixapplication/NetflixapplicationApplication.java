package com.example.netflixapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetflixapplicationApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetflixapplicationApplication.class, args);
    }

}
