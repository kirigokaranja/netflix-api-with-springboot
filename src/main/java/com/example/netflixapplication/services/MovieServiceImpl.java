package com.example.netflixapplication.services;

import com.example.netflixapplication.Repositories.MovieRepository;
import com.example.netflixapplication.models.Category;
import com.example.netflixapplication.models.Movie;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Movie createMovie(Movie movie, Long userId) {
        return null;
    }

    @Override
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    @Override
    public List<Movie> findMovieByCategoryandType(Category category, String type) {
        return movieRepository.findAllByMovieTypeAndCategories(type, category);
    }

    @Override
    public Movie updateMovie(Long id, Movie movie, Long userId) {
        return null;
    }

    @Override
    public void deleteMovie(Long id, Long userId) {
    }
}
