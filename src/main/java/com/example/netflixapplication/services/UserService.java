package com.example.netflixapplication.services;

import com.example.netflixapplication.models.User;

import java.util.List;

public interface UserService {

    User createUser(User user);

    List<User> getAllUsers();

    User findByIdNumber(Long id);
}
