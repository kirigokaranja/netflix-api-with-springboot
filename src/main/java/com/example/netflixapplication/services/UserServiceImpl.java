package com.example.netflixapplication.services;

import com.example.netflixapplication.Repositories.UserRepository;
import com.example.netflixapplication.models.User;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findByIdNumber(Long id) {
        return userRepository.findByIdNumber(id);
    }
}
