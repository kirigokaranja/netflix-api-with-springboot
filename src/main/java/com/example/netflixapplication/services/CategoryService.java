package com.example.netflixapplication.services;

import com.example.netflixapplication.models.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    Category createCategory(Category category);

    List<Category> getAllCategories();

    Optional<Category> findById(Long id);
}
