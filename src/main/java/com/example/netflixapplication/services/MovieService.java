package com.example.netflixapplication.services;

import com.example.netflixapplication.models.Category;
import com.example.netflixapplication.models.Movie;

import java.util.List;

public interface MovieService {

    Movie createMovie(Movie movie, Long userId);

    List<Movie> getAllMovies();

    List<Movie> findMovieByCategoryandType(Category category, String type);

    Movie updateMovie(Long id, Movie movie, Long userId);

    void deleteMovie(Long id, Long userId);
}
