package com.example.netflixapplication.Repositories;

import com.example.netflixapplication.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByIdNumber(Long idNumber);
}
