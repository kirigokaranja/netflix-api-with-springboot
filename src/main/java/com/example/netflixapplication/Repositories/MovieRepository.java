package com.example.netflixapplication.Repositories;

import com.example.netflixapplication.models.Category;
import com.example.netflixapplication.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {

    List<Movie> findAllByMovieTypeAndCategories(String type, Category category);
}
