package com.example.netflixapplication.Repositories;

import com.example.netflixapplication.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
